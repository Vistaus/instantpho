/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import InstantFX.ImageProcessor 0.1
import QtQuick.Layouts 1.1

import "../filters" as Filters
import "../effects"
import "../components"

Page {
    id: cropPage
    anchors.fill: parent

    property alias imageSource: cropImg.source
    property alias __cropImg: cropImg

    property var cropRatios: [ cropImg.originalImageRatio, 1, 3/2, 4/3, 16/9, 2/3, 3/4, 9/16 ]
    property var cropRatioLabels: [ i18n.tr("Original"), "1:1", "3:2", "4:3", "16:9", "2:3", "3:4", "9:16" ]

    Item {
        anchors {
            top: cropPage.header.bottom
            left: parent.left
            right: parent.right
            bottom: bottomPanel.top
        }

        CropAreaHighlight {
            id: overlay

            anchors.centerIn: parent
            width: Math.min(parent.width, units.gu(60))
            height: Math.min(parent.height, units.gu(60))

            z: 100

            CropAreaImage {
                id: cropImg
                overlay: overlay
                anchors.fill: overlay.rect
               // clip: true
                z: -100
            }
        }

        // TODO: Dovrebbe scurire soltanto la porzione di immagine fuori dall'overlay, non anche lo sfondo della pagina.
        Rectangle {
            id: ov
            parent: overlay.rect
            anchors { fill: parent; margins: -ov.border.width }
            color: "transparent"
            opacity: 0.3
            border {
                color: "black"
                width: Math.max(mainView.width, mainView.height)
            }
        }

  /*      Rectangle {
            anchors {
                right: parent.right
                bottom: parent.bottom
                top: parent.top
            }
            width: units.gu(4)
            color: Qt.rgba(1.0, 1.0, 1.0, 0.5)
            z: 1000

            // TODO: Wait for a vertical Slider from UITK
            Slider {
                id: zoomSlider
                anchors.centerIn: parent
                rotation: -90

                minimumValue: 1.0
                maximumValue: 5.0
                value: cropImg.zoomFactor
                onValueChanged: cropImg.zoomFactor = value
                live: true
                Binding {
                    target: zoomSlider
                    property: "value"
                    when: !zoomSlider.pressed
                    value: cropImg.zoomFactor
                }
                style: SliderStyle {}
                visible: QuickUtils.mouseAttached
            }
        }*/
    }

    Item {
        id: bottomPanel
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: childrenRect.height

        PageHeader {
            anchors { left: parent.left; right: parent.right }
            rotation: 180

            contents: ListView {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }
                rotation: 180
                width: Math.min(parent.width, contentWidth)
                orientation: ListView.Horizontal
                interactive: contentWidth > width

                model: cropRatioLabels
                delegate: AbstractButton {
                    property bool isSelected: overlay.cropRatio == cropRatios[model.index]

                    width: units.gu(6)
                    height: parent.height

                    onClicked: overlay.cropRatio = cropRatios[model.index]

                    Label {
                        id: cropRatioLabel
                        anchors.centerIn: parent
                        textSize: Label.Small
                        text: modelData
                    }

                    Rectangle {
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            bottom: parent.bottom
                        }

                        width: Math.max(units.gu(4), cropRatioLabel.width)
                        height: units.dp(3)
                        color: theme.palette.selected.backgroundText
                        visible: isSelected
                    }
                }
            }
        }
    }

    header: PageHeader {
        leadingActionBar.anchors.leftMargin: 0
        leadingActionBar.delegate: TextualButtonStyle {}
        leadingActionBar.actions: Action {
            text: i18n.tr("Cancel")
            onTriggered: {
                // Resource optimizations for low-end devices
                mainView.cropTmp = {}
                pageStack.clear()
                pageStack.push(Qt.resolvedUrl("MainPage.qml"))

//                pageStack.pop()
            }
        }

        trailingActionBar.anchors.rightMargin: 0
        trailingActionBar.delegate: TextualButtonStyle {}
        trailingActionBar.actions: Action {
            text: i18n.tr("Next")
            onTriggered: {
                // Resource optimizations for low-end devices
                mainView.cropTmp = cropImg.exportParameters()
                pageStack.clear()
                pageStack.push(Qt.resolvedUrl("FiltersPage.qml"))

                var r = cropImg.getCropRect()

                proc.loadImage("image://photo/" + imageSource.toString().replace("file://", "") + "?crop=true" + "&x="+ r.x + "&y=" + r.y +"&w=" + r.width + "&h=" + r.height)
            }
        }
    }
}
