set(PLUGIN_DIR InstantFX/ImageProcessor)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)

file(GLOB_RECURSE QML_SRCS
    qml/*.qml
    qml/*.js
)


file(GLOB_RECURSE ASSETS_SRCS
    assets/*.vsh
    assets/*.fsh
    assets/*.png
)

add_custom_target(ucc-qt-creator-show-qmldir-file SOURCES qmldir)
add_custom_target(ucc-qt-creator-show-qml-files SOURCES ${QML_SRCS})
add_custom_target(ucc-qt-creator-show-shader-files SOURCES ${ASSETS_SRCS})

#add the sources to compile
set(ifx-imageprocessor_SRCS
    backend.cpp
    imageprocessor.cpp
    offscreenrenderer.cpp
    cropimageprovider.cpp
    procdeviceinfo.cpp
)

add_library(imageprocessorplugin MODULE
    ${ifx-imageprocessor_SRCS}
)

qt5_use_modules(imageprocessorplugin Qml Quick Gui Widgets)

# Copy the plugin, the qmldir file and other assets to the build dir for running in QtCreator
if(NOT "${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_BINARY_DIR}")
add_custom_command(TARGET imageprocessorplugin POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/../${PLUGIN_DIR}
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/qmldir ${CMAKE_CURRENT_BINARY_DIR}/../${PLUGIN_DIR}
    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:imageprocessorplugin> ${CMAKE_CURRENT_BINARY_DIR}/../${PLUGIN_DIR}
    COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/qml/* ${CMAKE_CURRENT_BINARY_DIR}/../${PLUGIN_DIR}
    COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/assets/* ${CMAKE_CURRENT_BINARY_DIR}/../${PLUGIN_DIR}
)
endif(NOT "${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_BINARY_DIR}")

# Install plugin file
set(QT_IMPORTS_DIR ${CMAKE_INSTALL_LIBDIR}/qt5/qml)
if(CLICK_MODE)
  set(QT_IMPORTS_DIR ${CMAKE_INSTALL_LIBDIR})
endif(CLICK_MODE)
install(TARGETS imageprocessorplugin DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN_DIR})
install(FILES qmldir DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN_DIR})
install(FILES ${QML_SRCS} DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN_DIR})
install(FILES ${ASSETS_SRCS} DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN_DIR})

add_test(
  NAME plugin-qmllint
  COMMAND qmllint ${QML_SRCS}
)
